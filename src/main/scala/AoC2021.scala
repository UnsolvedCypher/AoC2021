import java.awt.Toolkit
import java.awt.datatransfer.StringSelection
import scala.annotation.tailrec
import scala.collection.immutable.HashMap
import scala.io.Source

object AoC2021 {
  def getInputRow(day: Int): List[String] = {
    getInputLines(day).head.split("\\s+").toList
  }

  def getInputLines(day: Int): List[String] = {
    val source = Source.fromFile(f"day$day.txt")
    val result = source.getLines().toList
    source.close()
    result
  }

  object Int {
    def unapply(s: String): Option[Int] = {
      s.toIntOption
    }
  }

  object Long {
    def unapply(s: String): Option[Long] = {
      s.toLongOption
    }
  }

  object Day1 {
    def partOne: Int = {
      getInputLines(1)
        .map(_.toInt)
        .sliding(2)
        .count {item => item.head < item(1)}
    }

    def partTwo: Int = {
      getInputLines(1)
        .map(_.toInt)
        .sliding(3)
        .map(_.sum)
        .sliding(2)
        .count {item => item.head < item(1)}
    }

    def main: (Int, Int) = {
      (partOne, partTwo)
    }
  }

  object Day2 {
    sealed trait Reading
    case class Forward(x: Int) extends Reading
    case class Down(x: Int) extends Reading
    case class Up(x: Int) extends Reading
    object Reading {
      def unapply(s: String): Option[Reading] = s.split(" ").toList match {
        case "forward" :: List(Int(x)) => Some(Forward(x))
        case "up" :: List(Int(x)) => Some(Up(x))
        case "down" :: List(Int(x)) => Some(Down(x))
        case _ => throw new RuntimeException("Invalid input")
      }
    }

    case class Position(x: Int, y: Int) {
      def product: Int = x * y
    }

    @tailrec
    def partOne(lines: List[Reading])(position: Position = Position(0, 0)): Position = lines match {
      case reading :: rest =>
        val newPos = reading match {
          case Forward(n) => position.copy(x = position.x + n)
          case Up(n) => position.copy(y = position.y - n)
          case Down(n) => position.copy(y = position.y + n)
        }
        partOne(rest)(newPos)
      case Nil =>
        position
    }

    @tailrec
    def partTwo(lines: List[Reading])(position: Position = Position(0, 0), aim: Int = 0): Position = lines match {
      case reading :: rest =>
        val newAim = reading match {
          case Up(n) => aim - n
          case Down(n) => aim + n
          case _ => aim
        }
        val newPos = reading match {
          case Forward(n) => Position(position.x + n, position.y + (aim * n))
          case _ => position
        }
        partTwo(rest)(newPos, newAim)
      case Nil =>
        position
    }

    def main: (Int, Int) = {
      val lines = getInputLines(2).map { case Reading(r) => r }
      val partOneAnswer = partOne(lines)().product
      val partTwoAnswer = partTwo(lines)().product
      (partOneAnswer, partTwoAnswer)
    }
  }

  object Day3 {
    def mostCommon(lines: List[List[Int]], position: Int): Int = {
      val inPosition = lines.map(_(position))
      if (inPosition.sum.toFloat / inPosition.length >= 0.5) 1 else 0
    }

    def partOne: Int = {
      val lines = getInputLines(3).map(_.map(_.toString.toInt).toList)
      val gammaInts = Seq.range(0, lines.head.length).map(position => mostCommon(lines, position))
      val epsilonInts = gammaInts.map(item => if (item == 0) 1 else 0)

      val gamma = Integer.parseInt(gammaInts.mkString(""), 2)
      val epsilon = Integer.parseInt(epsilonInts.mkString(""), 2)
      gamma * epsilon
    }

    @tailrec
    def narrowDown(lines: List[List[Int]], useMostCommon: Boolean, position: Int = 0): Int = lines.length match {
      case 1 => Integer.parseInt(lines.head.mkString(""), 2)
      case _ =>
        val check = if (useMostCommon)
          (a: List[Int]) => a(position) == mostCommon(lines, position)
        else
          (a: List[Int]) => a(position) != mostCommon(lines, position)
        narrowDown(lines.filter(check), useMostCommon, position + 1)
    }

    def partTwo: Int = {
      val lines = getInputLines(3).map(_.map(_.toString.toInt).toList)
      val oxygen = narrowDown(lines, useMostCommon = true)
      val carbon = narrowDown(lines, useMostCommon = false)
      oxygen * carbon
    }

    def main: (Int, Int) = {
      (partOne, partTwo)
    }
  }

  object Day4 {
    sealed trait Number
    case class Marked(n: Int) extends Number
    case class Unmarked(n: Int) extends Number

    type Board = Array[Array[Number]]

    def isWinner(board: Board): Boolean = {
      val columns = Seq.range(0, board.head.length).map(colIndex => board.map(_(colIndex)))
      board.exists(row => row.forall(_.isInstanceOf[Marked])) ||
        columns.exists(col => col.forall(_.isInstanceOf[Marked]))
    }

    def boardScore(board: Board, currNum: Int): Int = {
      board.flatMap(_.map{
        case Marked(_) => 0
        case Unmarked(x) => x
      }).sum * currNum
    }

    def markBoard(number: Int)(board: Board): Board = {
      board.map(_.map {
        case Unmarked(x) if x == number => Marked(x)
        case other => other
      })
    }

    @tailrec
    def findWinner(boards: Array[Board], numbers: List[Int]): Int = numbers match {
      case currNum :: rest =>
        val markedBoards = boards.map(markBoard(currNum))
        val winners = markedBoards.filter(isWinner)
        winners.headOption match {
          case Some(winner) => boardScore(winner, currNum)
          case None => findWinner(markedBoards, rest)
        }
      case Nil =>
        throw new RuntimeException("No winners")
    }

    @tailrec
    def findLoser(boards: Array[Board], numbers: List[Int]): Int = {
      val newBoards = boards.map(markBoard(numbers.head)).filter(!isWinner(_))
      if (newBoards.length == 1)
        findWinner(newBoards, numbers)
      else {
        findLoser(newBoards, numbers.tail)
      }
    }

    def main: (Int, Int) = {
      val input = getInputLines(4).mkString("\n")
      val numbers = input.split("\n").head.split(",").map(_.toInt).toList
      val boards: Array[Board] = input
        .split("\n\n")
        .tail
        .map(_.split("\n")
          .map(_.trim.split("\\s+")
            .map(x => Unmarked(x.toInt))))
      (findWinner(boards, numbers), findLoser(boards, numbers))
    }
  }

  object Day5 {
    case class VentEntry(from: (Int, Int), to: (Int, Int))
    object VentEntry {
      def unapply(s: String): Some[VentEntry] = {
        def fragmentToTuple(fragment: String): (Int, Int) = {
          (fragment.split(",")(0).toInt, fragment.split(",")(1).toInt)
        }

        Some(VentEntry(from=fragmentToTuple(s.split(" -> ")(0)), to=fragmentToTuple(s.split(" -> ")(1))))
      }
    }

    type Ocean = Array[Array[Int]]

    def moveInDirection(a: Int, b: Int): Int = {
      if (a == b)
        a
      else if (a < b)
        a + 1
      else
        a - 1
    }

    @tailrec
    def mapLine(ocean: Ocean, start: (Int, Int), end: (Int, Int)): Ocean = {
      val currValue = ocean(start._1)(start._2)
      val newRow = ocean(start._1).updated(start._2, currValue + 1)
      val newOcean = ocean.updated(start._1, newRow)

      if (start._1 == end._1 && start._2 == end._2)
        newOcean
      else {
        val newX = moveInDirection(start._1, end._1)
        val newY = moveInDirection(start._2, end._2)
        mapLine(newOcean, (newX, newY), end)
      }
    }

    @tailrec
    def mapLines(ocean: Ocean, lines: List[VentEntry]): Ocean = lines match {
      case line :: rest =>
        val newOcean = mapLine(ocean, line.from, line.to)
        mapLines(newOcean, rest)
      case Nil =>
        ocean
    }

    def main: (Int, Int) = {
      val allLines = getInputLines(5).map { case VentEntry(e) => e }
      val isStraight = (entry: VentEntry) => entry.to._1 == entry.from._1 || entry.to._2 == entry.from._2
      val straightLines = allLines.filter(isStraight)

      val maxX = allLines.map(_.to._1).appendedAll(allLines.map(_.from._1)).max
      val maxY = allLines.map(_.to._2).appendedAll(allLines.map(_.from._2)).max

      val oceanPartOne = Array.ofDim[Int](maxX + 1, maxY + 1)
      val oceanPartTwo = Array.ofDim[Int](maxX + 1, maxY + 1)

      val part1 = mapLines(oceanPartOne, straightLines).flatten.count(_ >= 2)
      val part2 = mapLines(oceanPartTwo, allLines).flatten.count(_ >= 2)
      (part1, part2)
    }
  }

  object Day6 {
    @tailrec
    def countFish(fish: Vector[Long], daysLeft: Int): Long = {
      val newFish: Vector[Long] = fish.tail.appended(fish.head)
        .updated(6, fish.head + fish(7))
      if (daysLeft > 0)
        countFish(newFish, daysLeft - 1)
      else
        fish.sum
    }

    def main: String = {
      val fish = getInputLines(6).head.split(",").map(_.toInt).toList
      val partOne = countFish(Vector.range(0, 9).map(i => fish.count(_ == i)), 80).toString
      val partTwo = countFish(Vector.range(0, 9).map(i => fish.count(_ == i)), 256).toString
      (partOne, partTwo).toString
    }
  }

  object Day7 {
    def partOne(values: Vector[Int]): Int = {
      val sorted = values.sorted
      val median = sorted(values.length / 2)
      values.map(v => Math.abs(v - median)).sum
    }

    def costForMiddle(middle: Int, numbers: Vector[Int]): Long = {
      val sumDigitsTo = (n: Long) => n * (n + 1) / 2
      numbers.map(n => sumDigitsTo(Math.abs(middle - n))).sum
    }

    def partTwo(values: Vector[Int]): Long = {
      Seq.range(values.min, values.max).map(v => costForMiddle(v, values)).min
    }

    def main: String = {
      val values = getInputLines(7).head.split(",").map(_.toInt).toVector
      (partOne(values), partTwo(values)).toString
    }
  }

  object Day8 {
    def decodeLine(line: InputLine): Long = {
      val lengthN = (n: Int) => line.patterns.filter(_.size == n)
      // Find by unique length
      val one = lengthN(2).head
      val four = lengthN(4).head
      val seven = lengthN(3).head
      val eight = lengthN(7).head

      // Do some fancy tricks
      val nine = lengthN(6).find(four.subsetOf(_)).get
      val zero = lengthN(6).find(s => seven.subsetOf(s) && s != nine).get
      val six = lengthN(6).find(s => s != nine && s != zero).get
      val two = lengthN(5).find(!_.subsetOf(nine)).get
      val three = lengthN(5).find(seven.subsetOf(_)).get
      val five = lengthN(5).find(_.subsetOf(six)).get

      val decoder = Map(
        zero -> 0,
        one -> 1,
        two -> 2,
        three -> 3,
        four -> 4,
        five -> 5,
        six-> 6,
        seven -> 7,
        eight -> 8,
        nine -> 9,
      )

      val sum = line.outputs.map(decoder).mkString("").toLong
      sum
    }

    case class InputLine(patterns: Seq[Set[Char]], outputs: Vector[Set[Char]])

    def main: (Long, Long) = {
      val lines = getInputLines(8)
        .map(
          _.split(" \\| ")).map(part => InputLine(
            patterns = part(0).split(" ").map(_.toSet).toVector,
            outputs = part(1).split(" ").map(_.toSet).toVector
      ))

      val decoded = lines.map(decodeLine)
      val partOne = decoded.map(_.toString.count(c => Vector('1', '4', '7', '8').contains(c))).sum
      val partTwo = decoded.sum
      (partOne, partTwo)
    }
  }

  object Day9 {
    case class Point(value: Int, row: Int, col: Int) {
      def risk: Int = {
        value + 1
      }
    }

    def main: (Int, Int) = {
      val lines = getInputLines(9).zipWithIndex.toVector.map{
        case (row, rowIndex) =>
          row.zipWithIndex.toVector.map {
            case (item, colIndex) => Point(item.toString.toInt, rowIndex, colIndex)
          }
      }

      val answerOne = partOne(lines)
      val answerTwo = partTwo(lines)
      (answerOne, answerTwo)
    }

    def partOne(lines: Vector[Vector[Point]]): Int = {
      lines.flatten(_.filter(current =>
        getSurroundings(current.row, current.col, lines, withDiagonals = true).forall(_.value > current.value)
      )).map(_.risk).sum
    }

    @tailrec
    def basinForPoint(toExplore: List[Point], lines: Vector[Vector[Point]], basin: Set[Point]): Set[Point] = toExplore match {
      case Point(currValue, row, col):: rest =>
        val surroundings = getSurroundings(row, col, lines, withDiagonals = false)
        val surroundingsInBasin = surroundings.filter(s => s.value > currValue && s.value != 9)

        basinForPoint(rest ++ surroundingsInBasin, lines, basin ++ surroundingsInBasin)
      case Nil =>
        basin
    }

    def partTwo(lines: Vector[Vector[Point]]): Int = {
      val basins = lines.flatMap(_.map(point => basinForPoint(List(point), lines, Set(point))))
      val answer = basins.distinct.map(_.size).sorted.takeRight(3).product
      answer
    }

    def getSurroundings(row: Int, col: Int, lines: Vector[Vector[Point]], withDiagonals: Boolean): Vector[Point] = {
      val adjacent = Vector(
        lines.lift(row - 1).flatMap(_.lift(col)),
        lines.lift(row + 1).flatMap(_.lift(col)),
        lines.lift(row).flatMap(_.lift(col - 1)),
        lines.lift(row).flatMap(_.lift(col + 1)),
      )

      val diagonal = Vector(
        lines.lift(row - 1).flatMap(_.lift(col - 1)),
        lines.lift(row + 1).flatMap(_.lift(col + 1)),
        lines.lift(row + 1).flatMap(_.lift(col - 1)),
        lines.lift(row - 1).flatMap(_.lift(col + 1)),
      )

      val result = if (withDiagonals) adjacent ++ diagonal else adjacent
      result.flatten
    }
  }

  object Day10 {
    def scoreForInvalidChar(c: Char): Long = c match {
      case ')' => 3L
      case ']' => 57L
      case '}' => 1197L
      case '>' => 25137L
    }

    def symbolsMatch(open: Char, close: Char): Boolean = (open, close) match {
      case ('(', ')') | ('[', ']') | ('{', '}') | ('<', '>') => true
      case _ => false
    }

    @tailrec
    def partOne(line: List[Char], history: List[Char] = List.empty): Long = line match {
      case currChar :: remainingChars =>
        currChar match {
          case '(' | '[' | '{' | '<' =>
            partOne(remainingChars, currChar :: history)
          case ')' | ']' | '}' | '>' =>
            history match {
              case expectedOpenChar :: historyRest if symbolsMatch(expectedOpenChar, currChar) =>
                partOne(remainingChars, historyRest)
              case _ => scoreForInvalidChar(currChar)
            }
        }
      case Nil =>
        0L
    }

    def scoreForRemainingChar(c: Char): Long = c match {
      case '(' => 1L
      case '[' => 2L
      case '{' => 3L
      case '<' => 4L
    }

    @tailrec
    def remainingCharsScore(history: List[Char], currScore: Long = 0): Long = history match {
      case head :: rest =>
        remainingCharsScore(rest, (currScore * 5) + scoreForRemainingChar(head))
      case Nil =>
        currScore
    }

    @tailrec
    def partTwo(line: List[Char], history: List[Char] = List.empty): Long = line match {
      case currChar :: remainingChars =>
        currChar match {
          case '(' | '[' | '{' | '<' =>
            partTwo(remainingChars, currChar :: history)
          case ')' | ']' | '}' | '>' =>
            history match {
              case expectedOpenChar :: historyRest if symbolsMatch(expectedOpenChar, currChar) =>
                partTwo(remainingChars, historyRest)
              case _ => 0L
            }
        }
      case Nil =>
        remainingCharsScore(history )
    }

    def main: (Long, Long) = {
      val lines = getInputLines(10)
      val partOneAnswer = lines.map(l => partOne(l.toList)).sum
      val partTwoResults = lines.map(l => partTwo(l.toList)).filter(_ > 0).sorted
      val partTwoAnswer = partTwoResults(partTwoResults.length / 2)
      (partOneAnswer, partTwoAnswer)
    }
  }

  object Day11 {
    type Sea = Vector[Vector[Octopus]]

    case class Octopus(value: Int, x: Int, y: Int, flashes: Long, alreadyFlashed: Boolean) {
      def flash: Octopus = {
        this.copy(value = 0, flashes = flashes + 1, alreadyFlashed = true)
      }
      def increment: Octopus = {
        if (alreadyFlashed) this else this.copy(value = value + 1)
      }
      def resetAndIncrement: Octopus = {
        this.copy(value = value + 1, alreadyFlashed = false)
      }
    }

    @tailrec
    def processFlashes(sea: Sea): Sea = {
      sea.flatten.find(o => o.value > 9 && !o.alreadyFlashed) match {
        case Some(toFlash) =>
          val newSea = sea.map(_.map(octopus => {
            if (Math.abs(octopus.x - toFlash.x) <= 1 && Math.abs(octopus.y - toFlash.y) <= 1)
              if (octopus == toFlash) octopus.flash else octopus.increment
            else
              octopus
          }))
          processFlashes(newSea)
        case None => sea
      }
    }

    @tailrec
    def doStep(sea: Sea, stepsLeft: Int = 1): Sea = {
      if (stepsLeft > 0) {
        val incrementedSea = sea.map(_.map(_.resetAndIncrement))
        val flashedSea = processFlashes(incrementedSea)
        doStep(flashedSea, stepsLeft - 1)
      }
      else
        sea
    }

    @tailrec
    def partTwo(sea: Sea, stepsTaken: Long = 0): Long = {
      if (sea.forall(_.forall(_.alreadyFlashed)))
        stepsTaken
      else
        partTwo(doStep(sea), stepsTaken + 1)
    }

    def main: (Long, Long) = {
      val sea = getInputLines(11).zipWithIndex.toVector.map {
        case (row, rowIndex) =>
          row.zipWithIndex.toVector.map {
            case (item, colIndex) => Octopus(item.toString.toInt, rowIndex, colIndex, 0L, alreadyFlashed = false)
          }
      }

      val answerOne = doStep(sea, 100).map(_.map(_.flashes).sum).sum
      val answerTwo = partTwo(sea)

      (answerOne, answerTwo)
    }
  }

  object Day12 {
    case class CavePath(mostRecentCave: String, otherCaves: Set[String], visitedTwice: Boolean) {
      def visitCave(cave: String): CavePath = {
        CavePath(cave, otherCaves + mostRecentCave, visitedTwice = visitedTwice || cave.toLowerCase == cave && otherCaves.contains(cave))
      }
    }

    @tailrec
    def countPaths(caveMap: Map[String, List[String]], pathsToExplore: List[CavePath] = List(CavePath("start", Set.empty, visitedTwice = false)), completePathsSoFar: Long = 0, nextCavesFilter: (String, CavePath) => Boolean): Long = pathsToExplore match {
      case currPath :: otherPaths => {
        if (currPath.mostRecentCave == "end") {
          countPaths(caveMap, otherPaths, completePathsSoFar + 1, nextCavesFilter)
        } else {
          val possibleNextCaves = caveMap(currPath.mostRecentCave).filter(c => nextCavesFilter(c, currPath))
          val childPaths = possibleNextCaves.map(currPath.visitCave)
          countPaths(caveMap, childPaths ++: otherPaths, completePathsSoFar, nextCavesFilter)
        }
      }
      case Nil => completePathsSoFar
    }

    def main: (Long, Long) = {
      val input = getInputLines(12).map(_.split("-"))
      val inputReverse = input.map(_.reverse)
      val caveMap = (input ++: inputReverse).groupMap(_(0))(_(1))
      val partOneCaveFilter = (nextCave: String, path: CavePath) => nextCave.toUpperCase == nextCave || !path.otherCaves.contains(nextCave)
      val answerOne = countPaths(caveMap, nextCavesFilter = partOneCaveFilter)
      val partTwoCaveFilter = (nextCave: String, path: CavePath) => nextCave != "start" && (nextCave.toUpperCase == nextCave || !path.visitedTwice || !path.otherCaves.contains(nextCave))
      val answerTwo = countPaths(caveMap, nextCavesFilter = partTwoCaveFilter)
      (answerOne, answerTwo)
    }
  }

  object Day13 {
    case class Dot(x: Int, y: Int) {
      def foldAlongY(yLine: Int): Dot = {
        val newY = if (y < yLine) y else yLine - (y - yLine)
        this.copy(y = newY)
      }

      def foldAlongX(xLine: Int): Dot = {
        val newX = if (x < xLine) x else xLine - (x - xLine)
        this.copy(x = newX)
      }
    }

    @tailrec
    def doFold(folds: List[Array[String]], dots: Set[Dot]): Set[Dot] = folds match {
      case currFold :: otherFolds =>
        val newDots = currFold(0) match {
          case "x" => dots.map(_.foldAlongX(currFold(1).toInt))
          case "y" => dots.map(_.foldAlongY(currFold(1).toInt))
        }
        doFold(otherFolds, newDots)
      case Nil => dots
    }

    def printDots(dots: Set[Dot]): Unit = {
      val dotX = dots.map(_.x)
      val dotY = dots.map(_.y)

      Seq.range(dotY.min, dotY.max + 1).foreach(y => {
        Seq.range(dotX.min, dotX.max + 1).foreach(x => {
          print(if (dots.contains(Dot(x, y))) "#" else ".")
        })
        println()
      })
    }

    def main: Int = {
      val lines = getInputLines(13).mkString("\n").split("\n\n")
      val dotLines = lines(0).split("\n")
      val dots = dotLines.map(l => {
        val split = l.split((","))
        Dot(split(0).toInt, split(1).toInt)
      }).toSet

      val folds = lines(1)
        .split("\n")
        .map(_.split(" ").last)
        .map(_.split("=")).toList

      val partOne = doFold(List(folds.head), dots).size

      printDots(doFold(folds, dots))
      partOne
    }
  }

  def runAll(): Unit = {
    List(
      Day1.main,
      Day2.main,
      Day3.main,
      Day4.main,
      Day5.main,
      Day6.main,
      Day7.main,
      Day8.main,
      Day9.main,
      Day10.main,
      Day11.main,
      Day12.main,
      Day13.main,
    ).zipWithIndex.foreach {
      case (f, index) => println(s"Part ${index + 1}: ${f}")
    }
  }

  def main(args: Array[String]): Unit = {
    val initialTime = System.nanoTime()
    runAll()
    val endTime = System.nanoTime()
    val totalTime = (endTime - initialTime).toDouble / 1000000000
    println(s"Took $totalTime seconds")
//    val answer = Day10.main.toString
//    val clipboard = Toolkit.getDefaultToolkit.getSystemClipboard
//    clipboard.setContents(new StringSelection(answer), null)
//    println(answer)
  }
}
